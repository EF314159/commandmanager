package shivanhunter.scrollergame.commands;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

import com.badlogic.gdx.InputProcessor;

public class CommandManager implements InputProcessor {
	private HashMap<Integer, String> keyCommands = new HashMap<Integer, String>();
	private HashMap<Integer, String> mouseCommands = new HashMap<Integer, String>();
	private String scrollDown = null, scrollUp = null;
	private String commandToAssign = null;
	
	private InputProcessor returnControlProcessor;
	private CommandProcessor processor;
	
	/**
	 * Constructs a CommandManager given a string of newline-delimited key mappings.
	 * The string can be empty or null, or contain lines containing key mappings such as:
	 * 
	 * MoveForward:key,51
	 * 
	 * Command names must not contain the ':' character or things will break. The
	 * mapping must be of the form "key,INTEGER" or "mouse,INTEGER" or "scrollup" or
	 * "scrolldown". Numbers will be from LibGDX Input.Keys constants.
	 * 
	 * This format is compatible with the output of CommandProcessor's toString() method.
	 */
	public CommandManager(String keyDefinitions) {
		if (keyDefinitions != null) {
			for (String line : keyDefinitions.split("[\\r\\n]+")) {
				String[] tokens = line.split(":");
				if (tokens.length < 2) continue;
				
				String[] args = tokens[1].trim().split(",");
				
				if (args[0].trim().equalsIgnoreCase("key"))
					keyCommands.put(Integer.parseInt(args[1].trim()), tokens[0].trim());
				
				if (args[0].trim().equalsIgnoreCase("mouse"))
					mouseCommands.put(Integer.parseInt(args[1].trim()), tokens[0].trim());
				
				if (args[0].trim().equalsIgnoreCase("scrollup")) scrollUp = tokens[0].trim();
				if (args[0].trim().equalsIgnoreCase("scrolldown")) scrollDown = tokens[0].trim();
			}
		}
	}
	
	/**
	 * Lists the command mappings in this CommandProcessor as a newline-delimited string
	 * containing command names and key mappings, such as:
	 * 
	 * MoveForward:key,51
	 * 
	 * This format is the same as the format used by CommandProcessor's constructor.
	 * Feeding this output into the constructor of a new CommandProcessor will create an
	 * identical CommandProcessor with the same mappings.  This can be used to easily
	 * save and load mappings into a keys.ini file (or similar).
	 */
	@Override public String toString() {
		String out = "";
		
		for (Integer i : keyCommands.keySet()) {
			out += keyCommands.get(i) + ":key," + i + "\n";
		}
		
		for (Integer i : mouseCommands.keySet()) {
			out += mouseCommands.get(i) + ":mouse," + i + "\n";
		}
		
		if (scrollUp != null) out += scrollUp + ":scrollup" + "\n"; 
		if (scrollDown != null) out += scrollDown + ":scrolldown" + "\n";
		
		return out;
	}
	
	/**
	 * Sets a CommandProcessor or CommandMultiplexer to distribute command events to.
	 * To distribute command events, this CommandManager (an InputProcessor) must be
	 * receiving the input events from LibGDX.
	 */
	public void setCommandProcessor(CommandProcessor p) {
		this.processor = p;
	}
	
	/**
	 * Instructs this CommandProcessor to bind the next key or mouse event to the
	 * given command name, then return control to the given InputProcessor. This
	 * should be your options menu's InputProcessor - there should be no problems
	 * using this same CommandProcessor for the options menu, but make sure the user
	 * can't "trap" themselves by removing the key bindings for navigating the menu!
	 * 
	 * This CommandProcessor must receive an input event to bind it, so either use
	 * Gdx.input.setInputProcessor() or add this CommandManager to your
	 * InputMultiplexer.
	 */
	public void waitFor(String command, InputProcessor processor) {
		commandToAssign = command;
		this.returnControlProcessor = processor;
	}
	
	/**
	 * Gets the human-readable string for a given keycode, such as "W" or "Enter".
	 * Good for displaying key mappings in an options menu. This is NOT the same
	 * format used for key bindings in toString() and the constructor. If there is
	 * no binding for the given command name, the string "-" is returned. If this is
	 * the command currently being rebound, "???" is returned.
	 */
	public String getKeyString(String command) {
		if (command == null) return "-";
		if (command == commandToAssign) return "???";
		
		if (scrollUp != null && scrollUp.equals(command)) return "MWheelUp";
		if (scrollDown != null && scrollDown.equals(command)) return "MWheelDown";
		
		for (Integer i : keyCommands.keySet()) {
			if (keyCommands.get(i).equals(command)) return Keys.toString(i);
		}
		
		for (Integer i : mouseCommands.keySet()) {
			if (mouseCommands.get(i).equals(command)) return "Mouse" + (i+1); 
		}
		
		return "-";
	}
	
	/**
	 * Removes the binding for a given command.
	 */
	private void removeCommand(String command) {
		int toRemove = -1;
		
		for (Integer i : keyCommands.keySet()) {
			if (keyCommands.get(i).equals(command)) toRemove = i;
		}
		
		keyCommands.remove(toRemove);
		toRemove = -1;
		
		for (Integer i : mouseCommands.keySet()) {
			if (mouseCommands.get(i).equals(command)) toRemove = i;
		}
		
		mouseCommands.remove(toRemove);
		
		if (scrollDown != null && scrollDown.equals(command)) scrollDown = null;
		if (scrollUp != null && scrollUp.equals(command)) scrollUp = null;
	}
	
	private boolean commandDown(String command) {
		if (processor != null && command != null) {
			return processor.commandDown(command);
		}
		return false;
	}
	
	private boolean commandUp(String command) {
		if (processor != null && command != null) {
			return processor.commandUp(command);
		}
		return false;
	}

	@Override public boolean keyDown(int keycode) {
		if (commandToAssign == null) {
			return commandDown(keyCommands.get(keycode));
		}
		return false;
	}

	@Override public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if (commandToAssign == null) {
			return commandDown(mouseCommands.get(button));
		}
		return false;
	}

	@Override public boolean keyUp(int keycode) {
		if (commandToAssign != null) {
			if (keycode != Keys.ESCAPE) {
				removeCommand(commandToAssign);
				keyCommands.put(keycode, commandToAssign);
			}
			if (returnControlProcessor != null) Gdx.input.setInputProcessor(returnControlProcessor);
			commandToAssign = null;
			return true;
		}
		
		else return commandUp(keyCommands.get(keycode));
	}

	@Override public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		if (commandToAssign != null) {
			removeCommand(commandToAssign);
			mouseCommands.put(button, commandToAssign);
			commandToAssign = null;
			if (returnControlProcessor != null) Gdx.input.setInputProcessor(returnControlProcessor);
			return true;
		}
		
		else return commandUp(mouseCommands.get(button));
	}

	@Override public boolean scrolled(int amount) {
		if (amount < 0) {
			if (commandToAssign != null) {
				removeCommand(commandToAssign);
				scrollUp = commandToAssign;
				commandToAssign = null;
				if (returnControlProcessor != null) Gdx.input.setInputProcessor(returnControlProcessor);
				return true;
			}
			
			else return commandUp(scrollUp);
			
		} else {
			if (commandToAssign != null) {
				removeCommand(commandToAssign);
				scrollDown = commandToAssign;
				commandToAssign = null;
				if (returnControlProcessor != null) Gdx.input.setInputProcessor(returnControlProcessor);
				return true;
			}
			
			else return commandUp(scrollDown);
		}
	}
	
	@Override public boolean touchDragged(int screenX, int screenY, int pointer) { return false; }
	@Override public boolean mouseMoved(int screenX, int screenY) { return false; }
	@Override public boolean keyTyped(char character) { return false; }
}
