package shivanhunter.scrollergame.commands;

public interface CommandProcessor {
	public boolean commandDown(String command);
	public boolean commandUp(String command);
}
