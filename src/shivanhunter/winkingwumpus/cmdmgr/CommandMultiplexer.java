package shivanhunter.scrollergame.commands;

import java.util.LinkedList;
import java.util.List;

public class CommandMultiplexer implements CommandProcessor {
	private List<CommandProcessor> processors = new LinkedList<CommandProcessor>();
	
	public void addProcessor(CommandProcessor processor) {
		processors.add(processor);
	}
	
	public void removeProcessor(CommandProcessor processor) {
		processors.remove(processor);
	}
	
	public void clear() {
		processors.clear();
	}

	@Override public boolean commandDown(String command) {
		for (CommandProcessor processor : processors) {
			if (processor.commandDown(command)) return true;
		}
		
		return false;
	}

	@Override public boolean commandUp(String command) {
		for (CommandProcessor processor : processors) {
			if (processor.commandUp(command)) return true;
		}
		
		return false;
	}
}
