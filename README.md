LibGDX CommandManager
=================

CommandManager is a very minimal library to help with key rebinding in LibGDX. a CommandManager will accept input events such as "Spacebar pressed" and "Right mouse button released" from LibGDX and send commands such as "Move forward pressed" and "Fire lasers released" to your code. You can easily bind commands to any key or mouse button.

Creating a CommandManager looks like this:

    public static CommandManager commandManager = new CommandManager(
    		"MoveUp:key,19\n" +
    		"MoveDown:key,20\n" +
    		"MoveLeft:key,21\n" +
    		"MoveRight:key,22\n" +
    		"Fire:key,129\n");

The CommandManager takes a newline-delimited string containing key bindings. It also has a toString() method that generates output in this format, so you can easily save/load bindings to a file.

There's very little checking on the validity of this string. If the command name contains a ':' character, things will probably just silently break.

The CommandManager is an InputProcessor, so to receive commands, you'll need to:

* Write your own class(es) to implement CommandProcessor and add the commandDown() and commandUp() events
* Add your class (or a CommandMultiplexer) to a CommandManager, using CommandManager.setCommandProcessor()
* Set LibGDX's InputProcessor to your CommandManager (or an InputMultiplexer containing it).

You can also easily rebind commands using a CommandManager. Suppose you have a button or menu option taht will rebind the "Move Up" key. When that button is clicked, you should run this code:

	yourCommandManager.waitFor("MoveUp", yourInputProcessor);
	Gdx.input.setInputProcessor(yourCommandManager);

This will put yourCommandManager into a state where the next input it receives from a key or mouse button will be accepted as the new key for the "MoveUp" command (unless the key is ESCAPE, in which case the bind is cancelled). The commandManager needs to be set as LibGDX's inputProcessor so it can actually receive that event. Once it does, it will return control to yourInputProcessor (since you're probably not using the CommandManager for the options menu).

(There's nothing technically wrong with using the same CommandManager to navigate the options menu, except the possibility of unbinding the keys used to navigate the menu. In that case, you could ignore the SetInputProcessor line and pass null into the waitFor command.)

As mentioned previously, saving and loading commands to a file is very easy. The toString() method generates a compatible (and mostly human-readable) list of commands which is easy to write to a file:

	FileHandle file = Gdx.files.local("keys.ini");
	file.writeString(myCommandManager.toString(), false);

And load again when the game starts:

	FileHandle file = Gdx.files.local("keys.ini");

	if (file.exists()) {
		myCommandManager = new CommandManager(file.readString());
	}
	else {
		myCommandManager = new CommandManager(
    		"MoveUp:key,19\n" +
    		"MoveDown:key,20\n" +
    		"MoveLeft:key,21\n" +
    		"MoveRight:key,22\n" +
    		"Fire:key,129\n");
	}
